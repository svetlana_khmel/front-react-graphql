import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { print } from 'graphql';
import gql from 'graphql-tag';
import axios from 'axios';

import { createApolloFetch } from 'apollo-fetch';

export class App extends Component {
  constructor() {
    super();

    this.getData = this.getData.bind(this);
    this.addBook = this.addBook.bind(this);
    this.updateBook = this.updateBook.bind(this);
    this.removeBook = this.removeBook.bind(this);
  }

  getData() {
    const fetch = createApolloFetch({
      uri: 'http://localhost:3000/graphql',
    });

    fetch({
      query: '{ books { _id title }}',
    }).then((res) => {
      console.log(res.data);
    });
  }

  removeBook() {
    const DELETE_BOOK = gql`
    mutation removeBook($id:String!) {
      removeBook(id: $id) {
        _id
      }
    }
    `;

    axios
      .post('http://localhost:3000/graphql', {
        query: print(DELETE_BOOK),
        variables: {
          id: String('5d42fc2bfdae67009d736e44')
        },
      })
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }

  updateBook() {
    const UPDATE_BOOK = gql`mutation addBook($id:String!, $title:String!, $description:String!) {
      updateBook(id:$id, title:$title, description:$description) {
        _id
        title
        description
      }
    }
    `;

    axios
      .post('http://localhost:3000/graphql', {
        query: print(UPDATE_BOOK),
        variables: {
          id: String('5d42f1c292988cf14ec1b27b'),
          title: 'blarrrrrrrr4534534',
          description: 'nnnnn',
        },
      })
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }


  addBook() {
    const ADD_BOOK = gql`mutation addBook($title:String!, $description:String!) {
      addBook(title:$title, description:$description) {
        title
        description
      }
    }
    `;

    axios
      .post('http://localhost:3000/graphql', {
        query: print(ADD_BOOK),
        variables: {
          title: 'bla'+Date.now(),
          description: 'nnnnn',
        },
      })
      .then(res => console.log(res))
      .catch(err => console.log(err));
  }


  render() {
    return (<div>
      <button onClick={this.getData}> Fetch </button>
      <button onClick={this.addBook}> Add </button>
      <button onClick={this.updateBook}> Update </button>
      <button onClick={this.removeBook}> Delete </button>
    </div>);
  }
}
