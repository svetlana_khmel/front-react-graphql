import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';

ReactDOM.render(
    <App />,
  // turned off eslint due to 'document' existence in render but not present in this file
  document.getElementById('root'), // eslint-disable-line
);

//registerServiceWorker();
